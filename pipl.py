from piplapis.search import SearchAPIRequest
from .config.config import PIPL_API

class Pipl(object):
    def __init__(self, email=None):
        if email == None:
            raise ValueError("Email not provided")
        self._email = email

    def get_details(self):
        try:
            request = SearchAPIRequest(email=self._email, api_key=PIPL_API)
            response = request.send()
            return response.to_dict()
        except Exception as e:
            print(e)
            return {"@http_status_code":500,"Error":"Internal Server Error", "Reason":str(e)}